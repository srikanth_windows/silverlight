﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.Runtime.Serialization;
using System.IO;
using System.Windows.Media.Imaging;
using GoogleAds;
using System.Diagnostics;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.ApplicationModel.Activation;
using Telerik.Windows.Controls;
using System.Threading.Tasks;

namespace YouTubeVedioDownloader
{
    public partial class savedVedio : PhoneApplicationPage
    {
        private InterstitialAd interstitialAd;
        SavedDataList parkinglistobj = new SavedDataList();
        IsolatedStorageFile Settings = IsolatedStorageFile.GetUserStoreForApplication();
        public savedVedio()
        {
            InitializeComponent();
            //AdView bannerAd = new AdView
            //{
            //    Format = AdFormats.Banner,
            //    AdUnitID = "ca-app-pub-9128294353107334/2138777804"
            //};
            //AdRequest adRequest = new AdRequest();
            //// Assumes we've defined a Grid that has a name
            //// directive of ContentPanel.
            //ContentPanel1.Children.Add(bannerAd);

            //bannerAd.LoadAd(adRequest);      //ca-app-pub-7696473424254342/5795608710





            try
            {
                interstitialAd = new InterstitialAd("ca-app-pub-7696473424254342/3061285111");
                AdRequest addRequest = new AdRequest();

                interstitialAd.ReceivedAd += OnAd1Received;
                interstitialAd.LoadAd(addRequest);
            }
            catch (Exception)
            {
            }





            getVedio();
        }
        private void OnAd1Received(object sender, AdEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Ad received successfully");

            interstitialAd.ShowAd();
        }
        private void OnAdReceived(object sender, AdEventArgs e)
        {
            Debug.WriteLine("Received ad successfully");
        }

        private void OnFailedToReceiveAd(object sender, AdErrorEventArgs errorCode)
        {
            Debug.WriteLine("Failed to receive ad with error " + errorCode.ErrorCode);
        }
        public class Vedio
        {
            public string ImagePath { get; set; }
            public string FileName { get; set; }
            public BitmapImage ImageView { get; set; }
            public String title { get; set; }
        }
        public static List<Vedio> source = new List<Vedio>();
        private void getVedio()
        {
            //using(IsolatedStorageFile isoFile = IsolatedStorageFile.GetUserStoreForApplication())
            //{
            //    foreach (string fileName in isoFile.GetFileNames())
            //    {
            //        source.Add(new Vedio() { FileName = fileName.ToString() });
            //    }
            //    this.myListBox.ItemsSource = source;
            //}
            try
            {
                parkinglistobj.Clear();
                source.Clear();

                if (Settings.FileExists("ParkignItemList"))
                {
                    using (IsolatedStorageFileStream fileStream = Settings.OpenFile("ParkignItemList", FileMode.Open))
                    {
                        DataContractSerializer serializer = new DataContractSerializer(typeof(SavedDataList));
                        parkinglistobj = (SavedDataList)serializer.ReadObject(fileStream);
                        foreach (var a in parkinglistobj.AsEnumerable().Reverse())
                        {
                            byte[] data;
                            int read;
                            BitmapImage bi;
                            using (IsolatedStorageFile isf = IsolatedStorageFile.GetUserStoreForApplication())
                            {

                                using (IsolatedStorageFileStream isfs = isf.OpenFile(a.ImagePath, FileMode.Open, FileAccess.Read))
                                {
                                    data = new byte[isfs.Length];
                                    while ((read = isfs.Read(data, 0, data.Length)) > 0)
                                    {
                                        //this.Dispatcher.BeginInvoke(() =>
                                        {
                                            try
                                            {
                                                if (isfs.Length != 0)
                                                    isfs.Read(data, 0, read);
                                            }
                                            catch (ObjectDisposedException e)
                                            {

                                            }
                                            //  
                                        }
                                        //);

                                    }
                                    MemoryStream ms = new MemoryStream(data);
                                    bi = new BitmapImage();
                                    bi.SetSource(ms);


                                }
                            }


                            //   Set bitmap source to memory stream


                            source.Add(new Vedio() { FileName = a.VedioPath, ImagePath = a.ImagePath, ImageView = bi, title = a.Title.ToString() });
                        }

                    }
                    myListBox.DataContext = null;
                    myListBox.ItemsSource = null;
                    this.myListBox.ItemsSource = source;
                }
            }
            catch (Exception e)
            {
            }
        }

        Vedio obj = null;
        private void myListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            obj = (sender as RadDataBoundListBox).SelectedItem as Vedio;
            if (obj != null)
            {


            }
        }
        string vedioName;
        private void playVed(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Vedio sndpitch = (sender as Border).DataContext as Vedio;
            //ListBoxItem pressItem = this.myListBox.ItemContainerGenerator.ContainerFromItem(sndpitch) as ListBoxItem;
            if (sndpitch != null)
            {
                vedioName = sndpitch.FileName.ToString();
                ContentPane.Visibility = Visibility.Visible;
            }
        }

        private void ContentPane_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ContentPane.Visibility = Visibility.Collapsed;
        }

        private async void Play_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            await Task.Delay(1000); //moje
            NavigationService.Navigate(new Uri("/play.xaml?file=" + vedioName, UriKind.Relative));
            ContentPane.Visibility = Visibility.Collapsed;
        }

        private void Delete_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ContentPane1.Visibility = Visibility.Visible;
        }

        private void tap_Yes(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var itemtoremove = parkinglistobj.Where(item => item.VedioPath == vedioName).First();
            parkinglistobj.Remove(itemtoremove);

            if (Settings.FileExists(vedioName))
            {
                Settings.DeleteFile(vedioName);
            }

            if (Settings.FileExists("ParkignItemList"))
            {
                Settings.DeleteFile("ParkignItemList");
            }
            using (IsolatedStorageFileStream fileStream = Settings.OpenFile("ParkignItemList", FileMode.Create))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(SavedDataList));
                serializer.WriteObject(fileStream, parkinglistobj);


            }

            getVedio();
            ContentPane.Visibility = Visibility.Collapsed;
            ContentPane1.Visibility = Visibility.Collapsed;
        }

        private void tap_NO(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ContentPane1.Visibility = Visibility.Collapsed;
        }

        private void Save_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (obj != null)
            {
                using (IsolatedStorageFile isoFile = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (isoFile.FileExists(vedioName))
                    {
                        var picker = new FileSavePicker();
                        picker.FileTypeChoices.Add("Video MP4", new List<string> { ".mp4" });
                        picker.ContinuationData["Operation"] = "SaveVideo";
                        picker.SuggestedFileName = obj.title;
                        picker.PickSaveFileAndContinue();
                    }
                }
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var app = App.Current as App;
            if (app.FilePickerContinuationArgs != null)
            {
                this.ContinueFileSavePicker(app.FilePickerContinuationArgs);
            }


            //myAd = new AdView();
            //myAd.AdUnitID = "ca-app-pub-9128294353107334/2138777804";
            //myAd.Height = 70;
            //myAd.Format = AdFormats.Banner;
            //myAd.VerticalAlignment = VerticalAlignment.Bottom;
            //myAd.FailedToReceiveAd += (o1, e1) =>
            //{
            //    Debug.WriteLine("\n\n\n" + e1.ErrorCode);
            //};

            //if (!ContentPanel1.Children.Contains(myAd))
            //    ContentPanel1.Children.Add(myAd);
            //AdRequest adRequest = new AdRequest();
            //myAd.LoadAd(adRequest);
        }

        public async void ContinueFileSavePicker(FileSavePickerContinuationEventArgs args)
        {
            try
            {
                if ((args.ContinuationData["Operation"] as string) == "SaveVideo" && args.File != null)
                {
                    if (obj != null)
                    {
                        using (var isoStore = IsolatedStorageFile.GetUserStoreForApplication())
                        {
                            using (var stream = isoStore.OpenFile(vedioName, System.IO.FileMode.Open))
                            {
                                byte[] buffer = new byte[16 * 1024];
                                int read;

                                using (var streamToWrite = await args.File.OpenStreamForWriteAsync())
                                {
                                    while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                                    {
                                        streamToWrite.Write(buffer, 0, read);
                                    }
                                }

                                MessageBox.Show("Video \"" + obj.title + "\" saved");

                                obj = null;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }

            (App.Current as App).FilePickerContinuationArgs = null;

        }

        private void myListBox_ItemReorderStateChanged(object sender, Telerik.Windows.Controls.ItemReorderStateChangedEventArgs e)
        {
            var list = new SavedDataList();
            var list1 = new List<Vedio>();
            foreach (var item in myListBox.ItemsSource)
            {
                var video = item as Vedio;
                list.Insert(0, new SavedData { ImagePath = video.ImagePath, Title = video.title, VedioPath = video.FileName });
                list1.Add(video);
            }

            if (Settings.FileExists("ParkignItemList"))
            {
                Settings.DeleteFile("ParkignItemList");
            }
            using (IsolatedStorageFileStream fileStream = Settings.OpenFile("ParkignItemList", FileMode.Create))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(SavedDataList));
                serializer.WriteObject(fileStream, list);
            }
            source = list1;
        }
    }
}