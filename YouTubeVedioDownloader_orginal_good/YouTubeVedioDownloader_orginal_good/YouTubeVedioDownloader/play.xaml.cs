﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Threading.Tasks;

namespace YouTubeVedioDownloader
{
    public partial class play : PhoneApplicationPage
    {
        public play()
        {
            InitializeComponent();
            

        }
        string ProdNa = "";
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            
            NavigationContext.QueryString.TryGetValue("file", out ProdNa);
            //PlayVideo(ProdNa);

            IsolatedStorageFile isoFile = null;
            try
            {
                using (isoFile = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (isoFile.FileExists(ProdNa))
                    {
                        var imageStream = isoFile.OpenFile(ProdNa, FileMode.Open, FileAccess.Read);
                        await Task.Delay(1000); //moje
                        player.SetSource(imageStream);
                    }
                }

            }
            catch (Exception ex)
            {

            }
        }

        async void PlayVideo(string ProdNa)
        {
            try
            {
                using (IsolatedStorageFile isoFile = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (isoFile.FileExists(ProdNa))
                    {
                        var imageStream = isoFile.OpenFile(ProdNa, FileMode.Open, FileAccess.Read);
                        await Task.Delay(1000); //moje
                        player.SetSource(imageStream);
                    }
                }

            }
            catch (Exception ex)
            {

            }
        }

        private void player_MediaEnded(object sender, Microsoft.PlayerFramework.MediaPlayerActionEventArgs e)
        {
            var item = savedVedio.source.FirstOrDefault(o => o.FileName == ProdNa);
            if (item != null)
            {
                var index = savedVedio.source.IndexOf(item);
                if (index >= 0)
                {
                    if (index + 1 < savedVedio.source.Count)
                    {
                        ProdNa = savedVedio.source[index + 1].FileName;
                        PlayVideo(ProdNa);
                    }
                }
            }
        }
    }
}