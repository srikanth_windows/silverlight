﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Threading;

namespace YouTubeVedioDownloader
{
    public partial class Splash : PhoneApplicationPage
    {
        private DispatcherTimer timer;
        public Splash()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(2);
            timer.Tick += (s, e) =>
            {
                var frame = App.Current.RootVisual as PhoneApplicationFrame;
                frame.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                timer.Stop();
            };
            timer.Start();
        }


    }
}