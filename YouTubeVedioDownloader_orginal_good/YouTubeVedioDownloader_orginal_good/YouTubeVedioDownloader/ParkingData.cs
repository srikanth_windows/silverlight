﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTubeVedioDownloader
{
    public class SavedData
    {
        public int ID { get; set; }
        public String VedioPath { get; set; }
        public String ImagePath { get; set; }
        public String Title { get; set; }

    }
    public class SavedDataList : List<SavedData>
    {
    }
}
