﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using YouTubeVedioDownloader.Resources;
using MyToolkit.Multimedia;
using System.Net.NetworkInformation;
using System.IO.IsolatedStorage;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Threading;
using GoogleAds;
using Windows.ApplicationModel.DataTransfer;
using Windows.ApplicationModel.Store;
using System.Threading.Tasks;
using Windows.Networking.BackgroundTransfer;
using Windows.Storage;

namespace YouTubeVedioDownloader
{
    public partial class MainPage : PhoneApplicationPage
    {
        public const string storagePath = "faisal";
        public static IsolatedStorageFile Settings = IsolatedStorageFile.GetUserStoreForApplication();
        SavedDataList parkinglistobj = new SavedDataList();
        private InterstitialAd interstitialAd;

        public static Windows.Storage.ApplicationDataContainer AppSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
        public const string KEY_LICENSE_CHECKED = "LICENSECHECKED";
        public const string KEY_ACTIVE = "ACTIVE";
        public static bool IsActive = false;
        public const string APP_NAME = "Free Mp3 Video Downloader";
        public const string IAP_ID = "unlock";

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            DownloadResultText.Text = "VEVO videos downloading may NOT working !";

            //ca-app-pub-7696473424254342/5795608710



            try
            {
                interstitialAd = new InterstitialAd("ca-app-pub-7696473424254342/5795608710"); //windows 1
                AdRequest addRequest = new AdRequest();

                interstitialAd.ReceivedAd += OnAd1Received;
                interstitialAd.LoadAd(addRequest);
            }
            catch (Exception)
            {
            }



            this.Loaded += SetParkingDetails_Loaded;

            string a = Microsoft.Phone.Info.DeviceStatus.DeviceName.ToString();
            MyBrowser.Source = new Uri("https://m.youtube.com/?", UriKind.Absolute);

            //DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
            //dataTransferManager.DataRequested += ShareData;
            LicenseChecked = AppSettings.Values.ContainsKey(KEY_LICENSE_CHECKED);
            IsActive = AppSettings.Values.ContainsKey(KEY_ACTIVE);

            //Utilities.IsActive = true; // TODO: remove this line before submit

        }
        private void OnAd1Received(object sender, AdEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Ad received successfully");
            interstitialAd.ShowAd();
        }
        private void OnAdReceived(object sender, AdEventArgs e)
        {
            Debug.WriteLine("Received ad successfully");
        }

        private void OnFailedToReceiveAd(object sender, AdErrorEventArgs errorCode)
        {
            Debug.WriteLine("Failed to receive ad with error " + errorCode.ErrorCode);
        }


        //IN APP
        private void ShareData(DataTransferManager sender, DataRequestedEventArgs e)
        {
            try
            {
                DataRequest request = e.Request;
                var deferral = request.GetDeferral();

                //request.Data.Properties.Title = DetailPage.CurrentVideo.Title;

                //var str = "";
                //if (DetailPage.CurrentVideo.Type == 0)
                //{
                //    str = " - https://www.youtube.com/watch?v=";
                //}

                //if (DetailPage.CurrentVideo.Type == 1)
                //{
                //    str = " - https://vimeo.com/";
                //}

                //if (DetailPage.CurrentVideo.Type == 2)
                //{
                //    str = " - http://www.dailymotion.com/video/";
                //}


                //var text = DetailPage.CurrentVideo.Title + str + DetailPage.CurrentVideo.VideoId;
                //request.Data.Properties.Description = text;
                //request.Data.SetText(text);
                deferral.Complete();
            }
            catch (Exception ex)
            {
            }
        }

        static bool LicenseChecked = false;

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!IsActive && !LicenseChecked && CurrentApp.LicenseInformation.ProductLicenses[IAP_ID].IsActive)
                {
                    IsActive = true;
                    AppSettings.Values[KEY_ACTIVE] = true;

                    //AppShell ash = new AppShell();
                    //ash.Remove_ads();
                }
                AppSettings.Values[KEY_LICENSE_CHECKED] = true;
            }
            catch (Exception)
            {
            }
            LicenseChecked = true;
        }




        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //string vedioFlieName = null;
            //string imageFlieName = null;
            //MyBrowser.Source = new Uri("https://m.youtube.com/?", UriKind.Absolute);
            BitmapImage bitmap = new BitmapImage(new Uri("/Images/back.png", UriKind.Relative));
            ImageBrush imagebrush = new ImageBrush();
            imagebrush.ImageSource = bitmap;
            bord1.Background = imagebrush;
            BitmapImage bitmap1 = new BitmapImage(new Uri("/Images/home.png", UriKind.Relative));
            ImageBrush imagebrush1 = new ImageBrush();
            imagebrush1.ImageSource = bitmap1;
            bord2.Background = imagebrush1;
            BitmapImage bitmap2 = new BitmapImage(new Uri("/Images/refresh.png", UriKind.Relative));
            ImageBrush imagebrush2 = new ImageBrush();
            imagebrush2.ImageSource = bitmap2;
            bord3.Background = imagebrush2;
            BitmapImage bitmap3 = new BitmapImage(new Uri("/Images/view_video.png", UriKind.Relative));
            ImageBrush imagebrush3 = new ImageBrush();
            imagebrush3.ImageSource = bitmap3;
            bord5.Background = imagebrush3;
        }


        private void SetParkingDetails_Loaded(object sender, RoutedEventArgs e)
        {
            //if (Settings.FileExists("ParkignItemList"))
            //{
            //    using (IsolatedStorageFileStream fileStream = Settings.OpenFile("ParkignItemList", FileMode.Open))
            //    {
            //        DataContractSerializer serializer = new DataContractSerializer(typeof(SavedDataList));
            //        parkinglistobj = (SavedDataList)serializer.ReadObject(fileStream);

            //    }
            //}

            if (Settings.FileExists("ParkignItemList"))
            {
                using (IsolatedStorageFileStream fileStream = Settings.OpenFile("ParkignItemList", FileMode.Open))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(SavedDataList));
                    parkinglistobj = (SavedDataList)serializer.ReadObject(fileStream);
                }
            }
        }

        string url;
        Uri lastUrl;
        string vedioId;
        DateTime unixEpoch;
        DateTime currentDate;
        string vedioFlieName = null;
        string imageFlieName = null;
        string vedioTitle;
        private void MyBrowser_Navigated(object sender, NavigationEventArgs e)
        {
            lastUrl = e.Uri;
            url = string.Empty;
            url = MyBrowser.Source.ToString();
        }

        private void download(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                if (NetworkInterface.GetIsNetworkAvailable())
                {




                    try
                    {
                        interstitialAd = new InterstitialAd("ca-app-pub-7696473424254342/9380236710");
                        AdRequest addRequest = new AdRequest();

                        interstitialAd.ReceivedAd += OnAd1Received;
                        interstitialAd.LoadAd(addRequest);
                    }
                    catch (Exception)
                    {
                    }





                    // https://m.youtube.com/watch?v=J6Qs3hXC2Co
                    if (lastUrl != new Uri("https://m.youtube.com/", UriKind.Absolute))
                    {
                        BitmapImage bitmapImage = new BitmapImage(new Uri("/Images/downloads1.png", UriKind.Relative));
                        ImageBrush imageBrush = new ImageBrush();
                        imageBrush.ImageSource = bitmapImage;
                        bord4.Background = imageBrush;
                        BitmapImage bitmap = new BitmapImage(new Uri("/Images/back.png", UriKind.Relative));
                        ImageBrush imagebrush = new ImageBrush();
                        imagebrush.ImageSource = bitmap;
                        bord1.Background = imagebrush;
                        BitmapImage bitmap1 = new BitmapImage(new Uri("/Images/home.png", UriKind.Relative));
                        ImageBrush imagebrush1 = new ImageBrush();
                        imagebrush1.ImageSource = bitmap1;
                        bord2.Background = imagebrush1;
                        BitmapImage bitmap2 = new BitmapImage(new Uri("/Images/refresh.png", UriKind.Relative));
                        ImageBrush imagebrush2 = new ImageBrush();
                        imagebrush2.ImageSource = bitmap2;
                        bord3.Background = imagebrush2;
                        BitmapImage bitmap3 = new BitmapImage(new Uri("/Images/view_video.png", UriKind.Relative));
                        ImageBrush imagebrush3 = new ImageBrush();
                        imagebrush3.ImageSource = bitmap3;
                        bord5.Background = imagebrush3;


                        string[] products = Regex.Split(url, "/?v=");
                        vedioId = products[1];
                        progResult.Visibility = Visibility.Visible;
                        progressBar.Visibility = Visibility.Visible;
                        unixEpoch = new DateTime(1970, 1, 1);
                        currentDate = DateTime.Now;
                        getYouTubeTitle();
                        SavedImages();
                        SavedVedio();

                    }
                    else
                    {
                        txtAlert.Text = "Please Select Vedio to Download...";
                        ContentPane1.Visibility = Visibility.Visible;
                        // MessageBox.Show("Please Select Vedio to Download...");
                    }

                }
                else
                {
                    //MessageBox.Show("You're not connected to Internet!");
                    txtAlert.Text = "You're not connected to Internet!";
                    ContentPane1.Visibility = Visibility.Visible;
                    NavigationService.GoBack();
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            //MyBrowser.Source = new Uri("https://m.youtube.com/?", UriKind.Absolute);
        }
        private async void SavedVedio()
        {

            //Get The Video Uri and set it as a player source         
            try
            {
                var url1 = await YouTube.GetVideoUriAsync(vedioId, YouTubeQuality.QualityLow);

                this.Dispatcher.BeginInvoke(() =>
                {
                    Uri a = url1.Uri;

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(a);
                    request.AllowReadStreamBuffering = false;
                    request.BeginGetResponse(new AsyncCallback(GetData), request);
                });
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                //YouTube.GetVideoUriAsync(vedioId, YouTubeQuality.QualityLow).Exception.ToString(); // RemoveHandler();
                //MessageBox.Show(YouTube.GetVideoUriAsync(vedioId, YouTubeQuality.QualityLow).Exception.ToString());

                //DeleteDownloads();

                //try
                //{
                //    await Task.Run(async () =>
                //            {
                //                var downloads = await BackgroundDownloader.GetCurrentDownloadsAsync();
                //                foreach (var download in downloads)
                //                {
                //                    CancellationTokenSource cts = new CancellationTokenSource();
                //                    await download.AttachAsync().AsTask(cts.Token);
                //                    cts.Cancel();
                //                }
                //                var localFolder = ApplicationData.Current.LocalFolder;
                //                var files = await localFolder.GetFilesAsync();
                //                files = files.Where(x => x.Name.EndsWith("_")).ToList();
                //                foreach (StorageFile file in files)
                //                {
                //                    await file.DeleteAsync(StorageDeleteOption.PermanentDelete);
                //                }
                //            });
                //}
                //catch (Exception)
                //{

                //    throw;
                //}
            }



        }


        public async void DeleteDownloads()
        {
            List<DownloadOperation> DownloadsList = new List<DownloadOperation>();
            DownloadOperation download;
            IReadOnlyList<DownloadOperation> downloads;
            downloads = await BackgroundDownloader.GetCurrentDownloadsAsync();
            if (downloads.Count > 0)
            {
                Debug.WriteLine("Delete Downloads");

                download = downloads.First(); // .First();

                try
                {

                    await download.ResultFile.DeleteAsync();
                    DownloadsList.Remove(download);
                    download = null;
                    //await download.AttachAsync().AsTask(token, progress);
                    //await download.StartAsync().AsTask(progress);
                }
                catch (TaskCanceledException ex)
                {
                    await download.ResultFile.DeleteAsync();
                    download = null;
                    Debug.WriteLine("Delete Downloads Error", ex);
                }

            }
        }

        private void GetData(IAsyncResult result)
        {                                      
           
            


            //using (IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication())
            //{
            //    using (IsolatedStorageFileStream streamToWriteTo = new IsolatedStorageFileStream("movie.wmv", FileMode.Create, file))
            //    {

            //if (!file.DirectoryExists(storagePath))
            //{
            //    file.CreateDirectory(storagePath);
            //}
            //string filePath = Path.Combine(storagePath, vedioFlieName);
            HttpWebRequest request = (HttpWebRequest)result.AsyncState;
            HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(result);

            long totalMiliSecond = (currentDate.Ticks - unixEpoch.Ticks) / 10000;

            vedioFlieName = string.Concat("mfk" + totalMiliSecond, ".mp4");
            try
            {
                using (IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream streamToWriteTo = new IsolatedStorageFileStream(vedioFlieName, FileMode.Create, file))
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            byte[] buffer = new byte[1024 * 8];
                            long totalValue = response.ContentLength;
                            int len;
                            while ((len = stream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                streamToWriteTo.Write(buffer, 0, buffer.Length);


                                try
                                {
                                    int count = 0;

                                    if (streamToWriteTo.Length > 0)
                                    {
                                        count = (int)((streamToWriteTo.Length * 100) / totalValue);
                                        var length = streamToWriteTo.Length;
                                        count = count + 1;
                                        Dispatcher.BeginInvoke(() =>
                                        {
                                            progressBar.Value = (double)length;
                                            progressBar.Maximum = (double)totalValue;
                                            DownloadResultText.Text = "Downloading: " + count + "% completed.";
                                        });
                                    }
                                }
                                catch (Exception)
                                {
                                }
                            }


                            //CopyStream(stream, streamToWriteTo);

                            ////str.Seek(0, SeekOrigin.Begin);

                            //byte[] buffer = new byte[8 * 1024];
                            //int len;
                            //int count;
                            //long totalValue = response.ContentLength;
                            //while ((len = stream.Read(buffer, 0, buffer.Length)) > 0)
                            //{
                            //    this.Dispatcher.BeginInvoke(() =>
                            //    {
                            //        try
                            //        {
                            //            if (len > 0)
                            //            {
                            //                streamToWriteTo.Write(buffer, 0, len);

                            //                count = 0;
                            //                if (streamToWriteTo.Length > 0)
                            //                    count = (int)((streamToWriteTo.Length * 100) / totalValue);

                            //                count = count + 1;

                            //                progressBar.Value = (double)streamToWriteTo.Length;
                            //                progressBar.Maximum = (double)totalValue;
                            //                DownloadResultText.Text = "Downloading: " + count + "% completed.";
                            //            }
                            //        }
                            //        catch (ObjectDisposedException e)
                            //        {

                            //        }
                            //    });
                            //}
                        }
                        //streamToWriteTo.Close();
                        //buffer = null;
                        // Debug.WriteLine("COMPLETED");
                    }
                }

                SavedListVedioDetails();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }

        private void SavedImages()
        {
            this.Dispatcher.BeginInvoke(() =>
            {


                string AuthService = "http://img.youtube.com/vi/" + vedioId + "/3.jpg";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(AuthService);
                request.AllowReadStreamBuffering = false;
                request.BeginGetResponse(new AsyncCallback(GetImageData), request);
            });
        }
        private void GetImageData(IAsyncResult result)
        {
            try
            {

                long totalMiliSecond = (currentDate.Ticks - unixEpoch.Ticks) / 10000;
                imageFlieName = string.Concat("img" + totalMiliSecond, ".jpeg");
                using (IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication())
                {


                    using (IsolatedStorageFileStream streamToWriteImg = new IsolatedStorageFileStream(imageFlieName, FileMode.Create, file))
                    {
                        HttpWebRequest request = (HttpWebRequest)result.AsyncState;
                        HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(result);

                        Stream str = response.GetResponseStream();

                        byte[] data = new byte[50 * 1024];
                        int read;

                        long totalValue = response.ContentLength;
                        while ((read = str.Read(data, 0, data.Length)) > 0)
                        {

                            // if (streamToWriteImg.Length != 0)

                            streamToWriteImg.Write(data, 0, read);

                        }

                        streamToWriteImg.Close();
                        data = null;
                        Debug.WriteLine("COMPLETED");

                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

        }

        private void getYouTubeTitle()
        {

            this.Dispatcher.BeginInvoke(() =>
            {
                string AuthServiceUri = "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=" + vedioId + "&fields=items(id%2Csnippet)&key=AIzaSyDeN5GOGby9ZM2M3w0Zh0nh6yF9bQd6DkA";
                HttpWebRequest spAthReq = HttpWebRequest.Create(AuthServiceUri) as HttpWebRequest;
                spAthReq.Accept = "application/json";
                spAthReq.Method = "GET";
                spAthReq.BeginGetResponse(new AsyncCallback(GetResponsetStreamCallback), spAthReq);
            });

        }
        private void GetRequestStreamCallback(IAsyncResult callback)
        {

            this.Dispatcher.BeginInvoke(() =>
            {
                HttpWebRequest myRequest = (HttpWebRequest)callback.AsyncState;

                myRequest.BeginGetResponse(new AsyncCallback(GetResponsetStreamCallback), myRequest);
            });

        }
        public class Default
        {
            public string url { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }

        public class Medium
        {
            public string url { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }

        public class High
        {
            public string url { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }

        public class Standard
        {
            public string url { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }

        public class Thumbnails
        {
            public Default @default { get; set; }
            public Medium medium { get; set; }
            public High high { get; set; }
            public Standard standard { get; set; }
        }

        public class Localized
        {
            public string title { get; set; }
            public string description { get; set; }
        }

        public class Snippet
        {
            public string publishedAt { get; set; }
            public string channelId { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public Thumbnails thumbnails { get; set; }
            public string channelTitle { get; set; }
            public List<string> tags { get; set; }
            public string categoryId { get; set; }
            public string liveBroadcastContent { get; set; }
            public Localized localized { get; set; }
        }

        public class Item
        {
            public string id { get; set; }
            public Snippet snippet { get; set; }
        }

        public class RootObject
        {
            public List<Item> items { get; set; }
        }
        private void GetResponsetStreamCallback(IAsyncResult callback)
        {
            try
            {
                string data;
                HttpWebRequest myhttpwebrequest = (HttpWebRequest)callback.AsyncState; ;
                using (HttpWebResponse response = (HttpWebResponse)myhttpwebrequest.EndGetResponse(callback))
                {
                    System.IO.Stream responseStream = response.GetResponseStream();
                    using (var reader = new System.IO.StreamReader(responseStream))
                    {
                        data = reader.ReadToEnd();
                    }
                    responseStream.Close();
                    var myObjects = JsonConvert.DeserializeObject<RootObject>(data);
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        foreach (var a in myObjects.items)
                        {
                            vedioTitle = a.snippet.localized.title.ToString();
                        }

                    });
                }
            }
            catch (Exception e)
            {

            }
        }
        private void SavedListVedioDetails()
        {
            try
            {


                this.Dispatcher.BeginInvoke(() =>
                {


                    if (imageFlieName != null && vedioFlieName != null)
                    {
                        int count = parkinglistobj.Count();
                        parkinglistobj.Add(new SavedData { ID = count + 1, VedioPath = vedioFlieName, ImagePath = imageFlieName, Title = vedioTitle.ToString() });
                        imageFlieName = null;
                        vedioFlieName = null;

                        if (Settings.FileExists("ParkignItemList"))
                        {
                            Settings.DeleteFile("ParkignItemList");
                        }
                        using (IsolatedStorageFileStream fileStream = Settings.OpenFile("ParkignItemList", FileMode.Create))
                        {
                            DataContractSerializer serializer = new DataContractSerializer(typeof(SavedDataList));
                            serializer.WriteObject(fileStream, parkinglistobj);
                            // MessageBox.Show("Download Completed..");
                            txtAlert.Text = "Download Completed...";
                            ContentPane1.Visibility = Visibility.Visible;
                            progResult.Visibility = Visibility.Collapsed;
                            progressBar.Visibility = Visibility.Collapsed;


                            BitmapImage bitmapImage = new BitmapImage(new Uri("/Images/downloads.png", UriKind.Relative));
                            ImageBrush imageBrush = new ImageBrush();
                            imageBrush.ImageSource = bitmapImage;
                            bord4.Background = imageBrush;
                            //BitmapImage bitmap = new BitmapImage(new Uri("/Images/view_video1.png", UriKind.Relative));
                            //ImageBrush imagebrush = new ImageBrush();
                            //imagebrush.ImageSource = bitmap;
                            //bord5.Background = imagebrush;
                            // MyBrowser.InvokeScript("eval", "history.go(-1)");


                        }
                    }
                });

            }
            catch (Exception ex)
            {

            }
        }

        private void pay(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MyBrowser.Source = new Uri("https://m.youtube.com/?", UriKind.Absolute);

            progressBar.Visibility = Visibility.Collapsed;
            progResult.Visibility = Visibility.Visible;
            DownloadResultText.Text = "";
            BitmapImage bitmap = new BitmapImage(new Uri("/Images/view_video1.png", UriKind.Relative));
            ImageBrush imagebrush = new ImageBrush();
            imagebrush.ImageSource = bitmap;
            bord5.Background = imagebrush;

            NavigationService.Navigate(new Uri("/savedVedio.xaml", UriKind.Relative));

        }

        private void back(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                interstitialAd = new InterstitialAd("ca-app-pub-7696473424254342/3061285111");
                AdRequest addRequest = new AdRequest();

                interstitialAd.ReceivedAd += OnAd1Received;
                interstitialAd.LoadAd(addRequest);
            }
            catch (Exception)
            {
            }

            BitmapImage bitmap = new BitmapImage(new Uri("/Images/back1.png", UriKind.Relative));
            ImageBrush imagebrush = new ImageBrush();
            imagebrush.ImageSource = bitmap;
            bord1.Background = imagebrush;
            BitmapImage bitmap1 = new BitmapImage(new Uri("/Images/home.png", UriKind.Relative));
            ImageBrush imagebrush1 = new ImageBrush();
            imagebrush1.ImageSource = bitmap1;
            bord2.Background = imagebrush1;
            BitmapImage bitmap2 = new BitmapImage(new Uri("/Images/refresh.png", UriKind.Relative));
            ImageBrush imagebrush2 = new ImageBrush();
            imagebrush2.ImageSource = bitmap2;
            bord3.Background = imagebrush2;
            BitmapImage bitmap3 = new BitmapImage(new Uri("/Images/view_video.png", UriKind.Relative));
            ImageBrush imagebrush3 = new ImageBrush();
            imagebrush3.ImageSource = bitmap3;
            bord5.Background = imagebrush3;
            MyBrowser.InvokeScript("eval", "history.go(-1)");

            //  mySleep();






        }
        private void mySleep()
        {
            Thread.Sleep(8000);

            BitmapImage bitmap1 = new BitmapImage(new Uri("/Images/back.png", UriKind.Relative));
            ImageBrush imagebrush1 = new ImageBrush();
            imagebrush1.ImageSource = bitmap1;
            bord1.Background = imagebrush1;
        }
        private void refresh(object sender, System.Windows.Input.GestureEventArgs e)
        {
            try
            {
                interstitialAd = new InterstitialAd("ca-app-pub-7696473424254342/3061285111");
                AdRequest addRequest = new AdRequest();

                interstitialAd.ReceivedAd += OnAd1Received;
                interstitialAd.LoadAd(addRequest);
            }
            catch (Exception)
            {
            }

            BitmapImage bitmap = new BitmapImage(new Uri("/Images/back.png", UriKind.Relative));
            ImageBrush imagebrush = new ImageBrush();
            imagebrush.ImageSource = bitmap;
            bord1.Background = imagebrush;
            BitmapImage bitmap1 = new BitmapImage(new Uri("/Images/home.png", UriKind.Relative));
            ImageBrush imagebrush1 = new ImageBrush();
            imagebrush1.ImageSource = bitmap1;
            bord2.Background = imagebrush1;
            BitmapImage bitmap2 = new BitmapImage(new Uri("/Images/refresh1.png", UriKind.Relative));
            ImageBrush imagebrush2 = new ImageBrush();
            imagebrush2.ImageSource = bitmap2;
            bord3.Background = imagebrush2;
            BitmapImage bitmap3 = new BitmapImage(new Uri("/Images/view_video.png", UriKind.Relative));
            ImageBrush imagebrush3 = new ImageBrush();
            imagebrush3.ImageSource = bitmap3;
            bord5.Background = imagebrush3;
            MyBrowser.Navigate(lastUrl);
        }

        private void home(object sender, System.Windows.Input.GestureEventArgs e)
        {

            try
            {
                interstitialAd = new InterstitialAd("ca-app-pub-7696473424254342/3061285111");
                AdRequest addRequest = new AdRequest();

                interstitialAd.ReceivedAd += OnAd1Received;
                interstitialAd.LoadAd(addRequest);
            }
            catch (Exception)
            {
            }

            BitmapImage bitmap = new BitmapImage(new Uri("/Images/back.png", UriKind.Relative));
            ImageBrush imagebrush = new ImageBrush();
            imagebrush.ImageSource = bitmap;
            bord1.Background = imagebrush;
            BitmapImage bitmap1 = new BitmapImage(new Uri("/Images/home1.png", UriKind.Relative));
            ImageBrush imagebrush1 = new ImageBrush();
            imagebrush1.ImageSource = bitmap1;
            bord2.Background = imagebrush1;
            BitmapImage bitmap2 = new BitmapImage(new Uri("/Images/refresh.png", UriKind.Relative));
            ImageBrush imagebrush2 = new ImageBrush();
            imagebrush2.ImageSource = bitmap2;
            bord3.Background = imagebrush2;
            BitmapImage bitmap3 = new BitmapImage(new Uri("/Images/view_video.png", UriKind.Relative));
            ImageBrush imagebrush3 = new ImageBrush();
            imagebrush3.ImageSource = bitmap3;
            bord5.Background = imagebrush3;
            MyBrowser.Source = new Uri("https://m.youtube.com/?", UriKind.Absolute);
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you Want to exit ?", "Exit from App!!", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                while (NavigationService.CanGoBack)
                {
                    //  MessageBox.Show("Do You Want to Exit ?");
                    NavigationService.RemoveBackEntry();
                }
            }
            else
            {
                NavigationService.Navigate(new Uri("/Mainpage.xaml", UriKind.Relative));
            }

        }

        private void tap_OK(object sender, System.Windows.Input.GestureEventArgs e)
        {
            ContentPane1.Visibility = Visibility.Collapsed;
        }


    }
}